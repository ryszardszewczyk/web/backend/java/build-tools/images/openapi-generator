SHELL := bash
DOCKERFILE := Dockerfile
DOCKER_CONTEXT := .
DOCKER_NAME = bananawhite98/openapi-generator
GITLAB_DOCKER_NAME = registry.gitlab.com/ryszardszewczyk/build-tools/images/openapi-generator/openapi-generator

VERSION := $(shell tag=$$(git describe --tags --abbrev=0 2>/dev/null); if [[ $$? -ne 0 ]]; then echo 0.0.$$(git rev-list --all --count); else echo $${tag}.$$(git rev-list $${tag}.. --count); fi;)
DOCKER_NAME_FULL = $(DOCKER_NAME):$(VERSION)
DOCKER_NAME_LATEST = $(DOCKER_NAME):1
PYTHON_IMAGE=bananawhite98/python-linux:1
PROJECT_ID = 58178104
REPOSITORY_ID = 6956485

.PHONY: docker-build-all

gitlab-build:
	docker build --pull -f $(DOCKERFILE) $(DOCKER_CONTEXT) -t $(GITLAB_DOCKER_NAME):$(TASK_NUMBER)

gitlab-push:
	docker push $(GITLAB_DOCKER_NAME):$(TASK_NUMBER)

gitlab-rmi:
	docker rmi -f $(GITLAB_DOCKER_NAME):$(TASK_NUMBER)

docker-build: $(DOCKERFILE)
	docker pull $(GITLAB_DOCKER_NAME):$(TASK_NUMBER)
	docker tag $(GITLAB_DOCKER_NAME):$(TASK_NUMBER) $(DOCKER_NAME_FULL)
	docker tag $(DOCKER_NAME_FULL) $(DOCKER_NAME_LATEST)

docker-push:
	docker push $(DOCKER_NAME_FULL)
	docker push $(DOCKER_NAME_LATEST)

docker-rmi:
	docker rmi -f $(GITLAB_DOCKER_NAME):$(TASK_NUMBER)
	docker rmi -f $(DOCKER_NAME_FULL)
	docker rmi -f $(DOCKER_NAME_LATEST)

docker-gitlab-rmi:
	docker run $(PYTHON_IMAGE) bash -c "gitlab-registry-docker.py --url '$(CI_SERVER_URL)' --token '$(CI_GITLABBOT)' --project-id $(PROJECT_ID) --repository-id $(REPOSITORY_ID) --tag '$(TASK_NUMBER)'"

print:
	@echo "openapi-generator image version is: $(VERSION)"

badge:
	docker run $(PYTHON_IMAGE) bash -c "images-badges.py --url '$(CI_SERVER_URL)' --token '$(CI_GITLABBOT)' --image openapi-generator --version $(VERSION) --project-id '$(CI_PROJECT_ID)' --base '$(CI_DEFAULT_BRANCH)'"; \

topic:
	docker run $(PYTHON_IMAGE) bash -c "images-topics.py --url '$(CI_SERVER_URL)' --token '$(CI_GITLABBOT)' --image openapi-generator --project_id '$(CI_PROJECT_ID)'";
