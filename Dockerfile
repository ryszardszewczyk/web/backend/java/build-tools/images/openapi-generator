FROM eclipse-temurin:21 AS base

RUN mkdir /opt/linters && cd /opt/linters \
    && apt-get update \
    && apt-get install -y --no-install-recommends npm nodejs unzip gcc git python3 python3-dev python3-pip \
    && pip3 install --no-cache-dir --break-system-packages tree_sitter==0.23.2 tree-sitter-java==0.23.2 \
    && npm install --ignore-scripts npm ibm-openapi-validator redoc-cli \
    && git clone --depth 1 https://github.com/tree-sitter/tree-sitter-java /tmp/tree-sitter-java \
    && rm -rf /var/lib/apt/lists/*

# NOSONAR
FROM eclipse-temurin:21 AS image

WORKDIR /opt/linters

ENV PATH="/opt/linters/node_modules/.bin:${PATH}"

RUN apt-get update \
    && apt-get install -y --no-install-recommends asciidoctor \
      graphviz  \
      ruby-asciidoctor-pdf  \
      gcc  \
      nodejs  \
      make \
      maven \
      wget \
      xmlstarlet \
      git \
      python3 \
      python3-dev \
      python3-pip \
      python3-ruamel.yaml \
      curl \
      vim \
      jq \
    && gem install asciidoctor-diagram \
    && pip3 install --no-cache-dir --break-system-packages anybadge tree_sitter==0.23.2 tree-sitter-java==0.23.2 \
    && V_OPENAPI=6.5.0 V_GJF=1.22.0 V_OPENAPI_5=5.0.0 V_NODE=18.16.0 \
    && wget --no-verbose "https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/${V_OPENAPI}/openapi-generator-cli-${V_OPENAPI}.jar" -O /usr/share/java/openapi-generator-cli.jar \
    && wget --no-verbose "https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/${V_OPENAPI_5}/openapi-generator-cli-${V_OPENAPI_5}.jar" -O /usr/share/java/openapi-generator-cli-5.jar \
    && wget --no-verbose "https://github.com/google/google-java-format/releases/download/v${V_GJF}/google-java-format-${V_GJF}-all-deps.jar" -O /usr/share/java/google-java-format.jar \
    && wget --no-verbose "https://nodejs.org/dist/v${V_NODE}/node-v${V_NODE}-linux-x64.tar.gz" -O node.tar.gz \
    && tar -xf node.tar.gz && rm -f node.tar.gz \
    && cp "node-v${V_NODE}-linux-x64/bin/node" /usr/local/bin && rm -rf "node-v${V_NODE}-linux-x64/" \
    && ln -s /usr/local/bin/node /usr/local/bin/nodejs \
    && apt-get -y remove gcc python3-dev \
    && apt-get -y clean autoclean \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

COPY bin/ /usr/bin/

RUN chmod +x /usr/bin/google-java-format /usr/bin/openapi-generator*

COPY opt/ /opt/
COPY --from=base /opt/linters .

RUN chmod +x /opt/makefile/*
