import re
from re import Pattern, compile
from pathlib import Path
from typing import Generator

BUILTIN_JAVA_TYPES = {
    "String": None,
    "Integer": None,
    "Long": None,
    "Boolean": None,
    "UUID": "java.util.UUID",
}


def rewrite_api(file: Path) -> None:
    """
    Rewrite the api file.
    :param file: File to rewrite.
    :return: None
    """
    print(f"Rewriting {file}...")
    with open(file, "r") as f:
        src: str = f.read()
    page_models: list = search_page.findall(src)

    imports: str = (
        "\nimport org.springframework.data.domain.Page;\n"
        "import org.springframework.data.domain.Pageable;\n"
        "import org.springframework.data.web.PageableDefault;\n"
        "import org.springframework.cloud.openfeign.SpringQueryMap;\n"
    )

    for model_name in page_models:
        class_name, qualified_class_name = get_model_data(model_name)
        src = src.replace(
            f"ResponseEntity<{model_name}>", f"ResponseEntity<Page<{class_name}>>"
        )
        if qualified_class_name:
            imports += "import " + qualified_class_name + ";\n"

    src = re.sub(pageable_regex, "@PageableDefault Pageable pageable", src)
    src = re.sub(pageable_regex2, "@PageableDefault Pageable pageable", src)
    src = re.sub(
        r"\* @param page.+\n\s*\* @param size.+\s*\* @param sort",  # NOSONAR
        "* @param pageable",
        src,
    )
    src = re.sub(pageable_regexp, "\\1, Pageable pageable);", src)
    src = re.sub(complex_filter_regex, "\\1 @SpringQueryMap \\2", src)
    src = re.sub("(package [^;]+;)", "\\1" + imports, src)

    with file.open("w+") as f:
        f.write(src)


def rewrite_model(file: Path) -> None:
    """
    Rewrite the model.
    :param file: File to rewrite.
    :return: None
    """
    src: str = file.read_text("utf-8")
    src = re.sub("import .+AllOf;", "", src)
    with file.open("w") as f:
        f.write(src)


def get_model_data(filename: str) -> tuple[str, str | None]:
    """
    Get the class name and qualified class name.
    :param filename: Name of file to read from.
    :return: Tuple of class name and qualified class name.
    """
    file: Path = list(src_dir.glob(f"**/{filename}.java"))[0]
    with file.open() as f:
        src: str = f.read()
    matches: list = search_content.findall(src)
    if not matches:
        raise ValueError(
            f"Page model {filename} doesn't contain field 'content' or it is not of array type."
        )
    content_model: str = matches[0]
    if content_model in BUILTIN_JAVA_TYPES:
        return content_model, BUILTIN_JAVA_TYPES[content_model]

    if content_model in BUILTIN_JAVA_TYPES:
        return content_model, BUILTIN_JAVA_TYPES[content_model]
    model_file = next(src_dir.glob(f"**/{content_model}.java"))
    return content_model, ".".join(model_file.parts[3:-1]) + "." + model_file.stem


src_dir: Path = Path("src")
search_page: Pattern[str] = compile(r"ResponseEntity<(\w+?Page)")
list_field: Pattern[str] = compile(r"(private List<)")
list_param: Pattern[str] = compile(r"(List<)")
search_content: Pattern[str] = compile(r"private List<(?:@Valid )?(\w+?)> content")
pageable_regex: Pattern[str] = compile(
    r'@Parameter\(name = "page".+?Integer page,.+?List<String> sort', flags=re.DOTALL
)
pageable_regex2: Pattern[str] = compile(
    r'(@Min\0\)\s+)?(@Valid\s+)?@RequestParam\(value = "page".+?List<String> sort',
    flags=re.DOTALL,
)
pageable_regexp: Pattern[str] = compile(
    r"(ResponseEntity<Page<.+?>> w\+\([^;]*@Valid \w+ query)\s*\);"
)
complex_filter_regex: Pattern[str] = compile(r"(@Valid) (w\+ query)")

controllers: Generator[Path, None, None] = src_dir.glob("**/*Api.java")
for controller in controllers:
    rewrite_api(controller)

models: Generator[Path, None, None] = src_dir.glob("**/model/*.java")
for model in models:
    rewrite_model(model)
