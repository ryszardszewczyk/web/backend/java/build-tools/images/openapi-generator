from __future__ import annotations

from logging import Logger, WARN, DEBUG, getLogger, StreamHandler
from re import findall, search
from subprocess import run, CompletedProcess, PIPE
import sys
from argparse import ArgumentParser, ArgumentTypeError, Namespace
from pathlib import Path


def init_logger(level: int = WARN) -> Logger:
    """
    Initialize a logger with specified level.
    :param level: Logging level
    :return: Logger instance
    """
    logger = getLogger(__file__)
    logger.setLevel(DEBUG)
    handler = StreamHandler()
    handler.setLevel(level)
    logger.addHandler(handler)
    return logger


def readeable_file_path(filename: str) -> Path:
    """
    Check if given filename exists and return absolute path.
    :param filename: Filename to check
    :return: Path to given file
    """
    path: Path = Path(filename)
    if not path.is_file():
        raise ArgumentTypeError(f"'{filename}' is not a file or does not exist.")
    return path


LOGGER = init_logger()


def parse_cli() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument(
        "-f",
        "--file",
        help="Path to the file which version should be calculated",
        required=True,
        type=readeable_file_path,
    )
    return parser.parse_args()


def file_git_version(cwd: Path, file: Path, submodule_dirs: list[str]) -> str:
    """
    Get the git version of a file.
    :param cwd: Current working directory
    :param file: File path
    :param submodule_dirs: Submodules
    :return: Calculated git version
    """
    path: str = str(file)
    for submodule_dir in submodule_dirs:
        if path.startswith(submodule_dir):
            path = submodule_dir
            break

    res: CompletedProcess[bytes] = run(
        [
            "git",
            "log",
            "-n",
            "1",
            "--pretty=format:%ct-%h",
            "--abbrev=10",
            "--",
            path,
        ],
        check=True,
        capture_output=True,
        cwd=cwd,
    )
    return res.stdout.decode(sys.stdin.encoding).strip()


def git_submodules() -> list[dict[str, str]]:
    """
    Get git submodules.
    :return: List of git submodules
    """
    res: CompletedProcess[bytes] = run(["git", "submodule"], check=True, stdout=PIPE)
    return list(
        map(parse_submodule_output, res.stdout.decode("utf8").strip().splitlines())
    )


def parse_submodule_output(lines: str) -> dict[str, str]:
    """
    Returns detailed information about git submodules.
    :param lines: List of git submodules
    :return: Git submodules information
    """
    columns: list[str] = lines.split()
    tokens: int = len(columns)
    return {
        "rev": columns[0],
        "path": " ".join(columns[1 : tokens - 1]),
        "head": columns[-1],
    }


def compute_version(cwd: Path, file: Path, mark_dirty: bool) -> str:
    """
    Compute the version of a file.
    :param cwd: Current working directory
    :param file: File path
    :param mark_dirty: Flag to mark file as dirty
    :return: String version of file
    """
    src_dir = file.parent
    submodule_dirs: list[str] = [submodule["path"] for submodule in git_submodules()]
    if mark_dirty:
        dirty_files: set[str] = set(
            run(
                ["git", "diff", "HEAD", "--name-only", str(src_dir)],
                check=True,
                capture_output=True,
                cwd=cwd,
            )
            .stdout.decode(sys.stdin.encoding)
            .strip()
            .split("\n")
        )
        dirty_files.discard("")
    text: str = (cwd / file).read_text()
    dependencies: set = set(findall(r"""\$ref:\s+['"]?([^#'"]+)""", text))
    if match := search(r"version: '?([\d.]+)'?", text):
        spec_version: str = match.group(1)
    else:
        raise ValueError(f'Could not find "version" in "{file}"')
    versions: set[str] = {
        spec_version + "-" + file_git_version(cwd, file, submodule_dirs)
    }

    for dependency in dependencies:
        dep_file: Path = cwd / src_dir / dependency
        versions.add(
            spec_version + "-" + file_git_version(cwd, dep_file, submodule_dirs)
        )
        compute_version(dep_file.parent.absolute(), Path(dep_file.name), mark_dirty)
    latest_version: str = max(versions)
    with (cwd / file.parent / (file.name.split(".")[0] + ".VERSION")).open(
        "w", encoding="utf-8"
    ) as fp:
        fp.write(latest_version)
    return latest_version


def main():
    args: Namespace = parse_cli()
    version: str = compute_version(Path.cwd(), args.file, mark_dirty=True)
    print(version)


if __name__ == "__main__":
    main()
