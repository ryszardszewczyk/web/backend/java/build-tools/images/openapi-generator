from argparse import ArgumentParser, ArgumentTypeError, BooleanOptionalAction, Namespace
from pathlib import Path
from typing import Any, Iterable
import json

from ruamel.yaml import YAML


def readable_file_path(filename: str) -> Path:
    """
    Converts the given filename to a Path object.
    :param filename: File path.
    :return: Path object.
    """
    path = Path(filename)
    if not path.is_file():
        raise ArgumentTypeError(f"{filename} doesn't exist or is not a file.")
    return path


def cli() -> Namespace:
    """
    Parses command line arguments.
    :return: Namespace object.
    """
    parser = ArgumentParser(description="Transforms OAS specification.")
    subparsers = parser.add_subparsers(required=True, dest="command")

    rewrite_parser = subparsers.add_parser(
        "rewrite", help="Rewrites OAS specification."
    )
    rewrite_parser.add_argument(
        "-i",
        "--input",
        required=True,
        type=readable_file_path,
        help="Input OAS specification.",
    )
    rewrite_parser.add_argument(
        "-o", "--output", required=True, type=Path, help="Output file."
    )
    rewrite_parser.add_argument(
        "--explode-get-parameters",
        action=BooleanOptionalAction,
        help="Should explode get parameters?",
    )
    rewrite_parser.add_argument(
        "--remove-pageable-parameters",
        action=BooleanOptionalAction,
        help="Should delete pageable parameters?",
    )

    external_defs_parser = subparsers.add_parser(
        "mappings",
        help="Saves external files used in specification relative to current directory.",
    )
    external_defs_parser.add_argument(
        "-i",
        "--input",
        required=True,
        type=readable_file_path,
        help="Input OAS specification.",
    )
    external_defs_parser.add_argument(
        "-o",
        "--output",
        required=True,
        type=Path,
        help="Output file.",
    )
    external_defs_parser.add_argument(
        "--exclude-files",
        required=False,
        nargs="*",
        default=[],
        help="Dictionary with ignored files which should not be assigned to library.",
    )
    external_defs_parser.add_argument(
        "--exclude-schemas",
        required=False,
        nargs="*",
        default=[],
        help="Dictionary with ignored schemas which should not be assigned to library.",
    )

    flag_parser = subparsers.add_parser(
        "mappings-flag", help="Writes flag from import-mappings for openapi-generator."
    )
    flag_parser.add_argument(
        "--generator-variant", required=True, help="Generator variant."
    )
    flag_parser.add_argument(
        "--mappings-file",
        required=True,
        type=readable_file_path,
        help="Files with external types metadata.",
    )

    mappings_deps_parser = subparsers.add_parser(
        "mappings-deps", help="Prints maven dependencies with external types."
    )
    mappings_deps_parser.add_argument(
        "--generator-variant", required=True, help="Generator variant."
    )
    mappings_deps_parser.add_argument(
        "--mappings-file",
        required=True,
        type=readable_file_path,
        help="Files with external types metadata.",
    )
    mappings_deps_parser.add_argument(
        "--root",
        required=True,
        type=Path,
        help="Directory which openapi-generator was called from.",
    )

    return parser.parse_args()


def leafs(d: dict[str, Any]) -> Iterable[tuple[str, Any]]:
    """
    Yields leaf keys and values from a dictionary.
    :param d: Dictionary.
    :return: Iterable of tuple.
    """
    for k, v in d.items():
        if isinstance(v, dict):
            yield from leafs(v)
        elif isinstance(v, list):
            for item in v:
                if isinstance(item, dict):
                    yield from leafs(item)
                else:
                    yield k, item
        else:
            yield k, v


def parse_openapi(specs: dict[Path, dict], filename: Path):
    """
    Parses OpenAPI specification.
    :param specs: Dictionary of OpenAPI specification as value and path to it as key.
    :param filename: Path to the OpenAPI specification.
    :return:
    """
    spec: dict[str, Any] = parse_yaml(filename)
    specs[filename.absolute()] = spec
    spec_dir: Path = filename.parent
    for k, v in leafs(spec):
        if k == "$ref":
            filename = v.partition("#")[0]
            if filename == "":
                continue
            sub_spec: Path = (spec_dir / filename).absolute()
            if sub_spec in specs:
                continue
            parse_openapi(specs, sub_spec)


def get_external_files(spec_filename: Path) -> set[Path]:
    """
    Returns set with paths to files that are used in specification.
    :param spec_filename: Path to the OpenAPI specification.
    :return: Set of paths to files that are used in specification.
    """
    external_references = set()
    spec: dict[str, Any] = parse_yaml(spec_filename)
    spec_dir: Path = spec_filename.parent
    for k, v in leafs(spec):
        if k == "$ref":
            filename: Any = v.partition("#")[0]
            if filename == "":
                continue
            sub_spec: Path = spec_dir / filename
            external_references.add(sub_spec)
    return external_references


def parse_yaml(filename: Path) -> dict[str, Any]:
    """
    Parses YAML specification.
    :param filename: File path.
    :return: Dictionary of parsed YAML specification.
    """
    yaml: YAML = YAML()
    yaml.preserve_quotes = True
    with filename.open("r", encoding="utf8") as fp:
        data: dict[str, Any] = yaml.load(fp)
    return data


def resolved_component(
    path: Path, ref: str, specs: dict[Path, dict]
) -> tuple[dict, Path]:
    """
    Resolves component reference.
    :param path: Path to the specification.
    :param ref: Reference path.
    :param specs: Dictionary of path to another specification and its parsed file.
    :return: Tuple of parsed component reference and path.
    """
    filename, _, schema_path = ref.partition("#/")
    schema_file = path
    if filename != "":
        schema_file = (path.parent / filename).absolute()
    spec: dict = specs[schema_file]
    for segment in schema_path.split("/"):
        spec: dict = spec[segment]
    return spec, schema_file


def relativize_ref(root: Path, path: Path, ref: str) -> str:
    """
    Returns relative reference to YAML file relative to given file.
    :param root: Root path.
    :param path: Path to the specification.
    :param ref: Reference to another specification.
    :return: Relative reference.
    """
    filename, _, component_path = ref.partition("#/")
    relative_path: Path = path.relative_to(root.parent)
    if filename:
        relative_path: Path = relative_path.parent / filename
    return f"{relative_path}#/{component_path}"


def flattenned_properties(  # NOSONAR
    parent: Path, path: Path, schema: dict, specs: dict[Path, dict], seen: set[int]
) -> tuple[dict[str, Any], set[str]]:
    """
    Returns a pair of dictionary with properties definitions, set of required fields from schema.
    :param parent: Path to parent.
    :param path: Path to specification.
    :param schema: Parsed specification.
    :param specs: Resolved external references.
    :param seen: Set of ids of already flattened specification properties.
    :return: Tuple of parsed specification and flags marking properties as required or not.
    """
    if schema_properties := schema.get("properties"):
        if parent != path and id(schema_properties) not in seen:
            seen.add(id(schema_properties))
            for property_name, schema_def in schema_properties.items():
                if schema_ref := schema_def.get("$ref"):
                    schema_def["$ref"] = relativize_ref(parent, path, schema_ref)
        return schema_properties, set(schema.get("required", []))
    if all_of := schema.get("allOf"):
        all_properties: dict[str, Any] = {}
        required = set()
        for all_of_schema in all_of:
            if ref := all_of_schema.get("$ref"):
                referenced_schema, schema_file = resolved_component(path, ref, specs)
                resolved_properties, resolved_required = flattenned_properties(
                    parent, schema_file, referenced_schema, specs, seen
                )
                all_properties |= resolved_properties
                required |= resolved_required
            elif all_of_properties := all_of_schema.get("properties"):
                all_properties |= all_of_properties
                required.update(all_of_schema.get("required", []))
        return all_properties, required
    raise ValueError(f"Unprocessed schema {schema}")


def explode_get_parameters(filename: Path, specs: dict[Path, dict]):  # NOSONAR
    """
    Rewrites specification exploding complex objects into a list of its fields.
    :param filename: Path to the specification.
    :param specs: Resolved external references.
    :return:
    """
    spec = specs[filename]
    seen_schemas: set[int] = set()
    for operations in spec.get("paths", {}).values():
        modified: bool = False
        get_operation: Any = operations.get("get", {})
        get_parameters: Any = get_operation.get("parameters", [])
        rewritten_parameters: list = []
        for parameter in get_parameters:
            if "$ref" not in parameter:
                rewritten_parameters.append(parameter)
                continue
            param, _ = resolved_component(filename, parameter["$ref"], specs)
            if param["in"] != "query":
                rewritten_parameters.append(parameter)
                continue
            explode: bool = param.get("explode", False)
            if explode:
                param_schema: Any = param["schema"]
                if "$ref" in param_schema:
                    param_schema, _ = resolved_component(
                        filename, param_schema["$ref"], specs
                    )
                if param_schema.get("type") == "array":
                    rewritten_parameters.append(parameter)
                    continue
                modified: bool = True
                schema_properties, required_properties = flattenned_properties(
                    filename, filename, param_schema, specs, seen_schemas
                )
                for k, v in schema_properties.items():
                    new_param = {
                        "name": k,
                        "in": "query",
                        "required": k in required_properties,
                        "schema": v,
                    }
                    if description := v.get("description"):
                        new_param["description"] = description
                    rewritten_parameters.append(new_param)
            else:
                rewritten_parameters.append(parameter)
        if modified:
            get_operation["parameters"] = rewritten_parameters


def remove_pageable_parameters(filename: Path, specs: dict[Path, dict]):  # NOSONAR
    """
    Removes pageable parameters from filter model.
    :param filename: Path to the specification.
    :param specs: Parsed specification.
    :return:
    """
    spec: dict = specs[filename]
    for parameter in spec.get("components", {}).get("parameters", {}).values():
        if not parameter.get("explode", False):
            continue
        schema: Any = parameter.get("schema", {})
        if "$ref" in schema:
            schema, _ = resolved_component(filename, schema["$ref"], specs)
        all_of: Any = schema.get("allOf", [])
        for i, all_of_def in enumerate(all_of):
            if all_of_def.get("$ref", "").endswith("PagedParams"):
                del all_of[i]
                break
    for path, operation in spec.get("paths", {}).items():
        for http_type, operation_def in operation.items():
            if not isinstance(operation_def, dict):
                continue
            parameters: list = operation_def.get("parameters", [])
            param_num: int = len(parameters)
            i = 0
            while i < param_num:
                parameter: Any = parameters[i]
                if ref := parameter.get("$ref"):
                    if ref.endswith("/PagedParamsOptional"):
                        print("Usuwanie parametrów stronicowania", http_type, path)
                        del parameters[i]
                        param_num -= 1
                        continue
                i += 1


def rewrite_spec(args: Namespace):
    """
    Rewrites specification.
    :param args: Parsed arguments.
    :return:
    """
    specs: dict[Path, dict] = {}
    filename: Path = args.input.absolute()
    parse_openapi(specs, filename)
    spec: dict = specs[filename]

    if args.explode_get_parameters:
        explode_get_parameters(filename, specs)
    if args.remove_pageable_parameters:
        remove_pageable_parameters(filename, specs)

    yaml: YAML = YAML()
    yaml.preserve_quotes = True
    yaml.dump(spec, args.output)


def generate_import_mapping(file: Path, excluded_schemas: list[str]) -> list[str]:
    """
    Generates import-mappings to use by openapi-generator.
    :param file: Path to the specification.
    :param excluded_schemas: List of excluded schemas.
    :return: List of import-mappings.
    """
    spec: dict[str, Any] = parse_yaml(file)
    defs = set()
    for component_type, component_def in spec.get("components", {}).items():
        if component_type in ("parameters", "responses"):
            continue
        for name, definition in component_def.items():
            if "additionalProperties" in definition:
                continue
            if not (
                definition.get("type") == "object"
                or "allOf" in definition
                or "enum" in definition
            ):
                continue
            defs.add(name)
    for excluded_schema in excluded_schemas:
        defs.discard(excluded_schema)
    return list(defs)


def generate_import_mappings(files: set[Path], excluded_schemas: list[str]):
    """
    Generates import-mappings for external types which have been generated to reusable library.
    :param files: Set of paths.
    :param excluded_schemas: List of excluded schemas.
    :return: Dict of import-mappings.
    """
    mappings = {}
    for file in files:
        mappings[str(file)] = generate_import_mapping(file, excluded_schemas)
    return mappings


def save_external_mappings(
    openapi_file: Path,
    mappings_file: Path,
    excluded_files: list[str],
    excluded_schemas: list[str],
):
    """
    Saves import-mappings to a given file.
    :param openapi_file: Path to the specification.
    :param mappings_file: Path to the file to save external mappings to.
    :param excluded_files: List of excluded files.
    :param excluded_schemas: List of excluded schemas.
    :return:
    """
    external_files: set[Path] = get_external_files(openapi_file)
    for file in excluded_files:
        external_files.discard(Path(file))
    mappings: dict[str, list[str]] = generate_import_mappings(
        external_files, excluded_schemas
    )
    with mappings_file.open("w", encoding="utf8") as fp:
        json.dump(mappings, fp, ensure_ascii=False)


def print_import_mappings_flag(mappings_file: Path, generator_variant: str):
    """
    On exit, saves a flag for openapi-generator that gives import-mappings.
    :param mappings_file: File with import-mappings.
    :param generator_variant: Generator variant.
    :return:
    """
    with mappings_file.open("r", encoding="utf8") as fp:
        mappings_data: Any = json.load(fp)
    for external_file, schemas in mappings_data.items():
        if not schemas:
            continue
        external_file_path: Path = Path(external_file)
        name: str = external_file_path.name.split(".")[0]
        external_config: dict[str, Any] = parse_yaml(
            external_file_path.parent / "config" / f"{name}-{generator_variant}.yml"
        )
        model_package: Any = external_config["modelPackage"]
        print("--import-mappings", end=" ")
        print(
            ",".join(map(lambda s: f"{s}:{model_package}.{s}", schemas)),  # NOSONAR
            end=" ",
        )


def print_import_mappings_deps(mappings_file: Path, generator_variant: str, root: Path):
    """
    On exit, prints maven dependencies from libraries with external types generated.
    :param mappings_file: Path to mappings file.
    :param generator_variant: Generator variant.
    :param root: Path to the root directory.
    :return:
    """
    with mappings_file.open("r", encoding="utf8") as fp:
        mappings_data: Any = json.load(fp)
    for external_file, schemas in mappings_data.items():
        if not schemas:
            continue
        external_file_path: Path = Path(external_file)
        name: str = external_file_path.name.split(".")[0]
        external_config: dict[str, Any] = parse_yaml(
            root
            / external_file_path.parent
            / "config"
            / f"{name}-{generator_variant}.yml"
        )
        version: str = (
            (root / external_file_path.parent / f"{name}.VERSION").read_text().strip()
        )
        group_id: Any = external_config["groupId"]
        artifact_id: Any = external_config["artifactId"]
        print(
            f"<dependency><groupId>{group_id}</groupId><artifactId>{artifact_id}</artifactId><version>{version}</version></dependency>",
            end="",
        )


def main():
    args = cli()
    match args.command:
        case "rewrite":
            rewrite_spec(args)
        case "mappings":
            save_external_mappings(
                args.input, args.output, args.exclude_files, args.exclude_schemas
            )
        case "mappings-flag":
            print_import_mappings_flag(args.mappings_file, args.generator_variant)
        case "mappings-deps":
            print_import_mappings_deps(
                args.mappings_file, args.generator_variant, args.root
            )


if __name__ == "__main__":
    main()
