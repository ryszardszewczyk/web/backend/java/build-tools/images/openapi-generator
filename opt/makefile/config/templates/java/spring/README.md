## [pojo.mustache](./pojo.mustache)

### Fixes

- added `@NotNull` on fields and setters.
- ignoring fields with `format: password` in toString method.
