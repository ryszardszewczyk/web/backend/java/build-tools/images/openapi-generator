import re
from pathlib import Path
from typing import Generator

from tree_sitter import Language, Node, Parser, Query
import tree_sitter_java as tsjava


def main():
    java_language: Language = Language(tsjava.language())

    java_parser: Parser = Parser()
    java_parser.language = java_language
    src_dir: Path = Path("src")
    controllers: Generator[Path, None, None] = src_dir.glob("**/*Api.java")
    for controller in controllers:
        rewrite_api(controller, java_parser, java_language)


def modify_method(
    method_node: Node, with_headers: bool, replace_file: bool
) -> tuple[bool, str]:
    """
    Modifies the method node.
    :param method_node: Method node
    :param with_headers: Headers flags
    :param replace_file: Replace file flag
    :return: Tuple with key as value from output_method function, and value of output_doc function as a string
    """
    for child in method_node.children:
        if child.type == "block":
            return False, ""
    doc_node: Node | None = method_node.prev_sibling
    doc = output_doc(doc_node, with_headers)
    method: list[str] = [doc, "\n"]
    returns_file: bool = False
    for child in method_node.children:
        parts: list[str] = []
        returns_file |= output_method(child, with_headers, replace_file, parts)
        method.extend(parts)

    method.append("\n")
    return returns_file, "".join(method)


def output_doc(node: Node, with_headers: bool) -> str:
    """
    Outputs doc string.
    :param node: Node to output
    :param with_headers: Headers to output
    :return: Returns doc string of the decoded node
    """
    text: str = node.text.decode("utf8")
    if not with_headers:
        return text
    parts: list[str] = text.split("\n")
    if "@return" in parts[-2]:
        parts.insert(-2, "   * @param headers Map with headers. (optional)")
    else:
        parts.insert(-1, "   * @param headers Map with headers. (optional)")
    return "\n".join(parts)


def output_method(  # NOSONAR
    node: Node, with_headers: bool, replace_file: bool, parts: list[str]
) -> bool:
    """
    Outputs method.
    :param node: Node to output
    :param with_headers: Headers to output
    :param replace_file: File to replace
    :param parts: Parts of the method
    :return: Returns True if node's children type of type identifier is File, otherwise returns False
    """
    uses_file: bool = False
    if node.type == "modifiers":
        parts.append(node.text.decode("utf8"))
        parts.append("\n")
    elif node.type == "type_identifier":
        text: str = node.text.decode("utf8")
        parts.append(text)
        parts.append(" ")
    elif node.type == "identifier":
        text: str = node.text.decode("utf8")
        if replace_file:
            if text.endswith("WithHttpInfo"):
                text = text.removesuffix("WithHttpInfo") + "StreamWithHttpInfo"
            else:
                text += "Stream"
        if with_headers:
            if text.endswith("WithHttpInfo"):
                text += "AndHeaders"
            else:
                text += "WithHeaders"
        parts.append(text)
    elif node.type == "formal_parameters":
        has_params: bool = len(node.children) != 2
        for param in node.children:
            if param.type == ")":
                if with_headers:
                    if has_params:
                        parts.append(", @HeaderMap Map<String, Object> headers)")
                    else:
                        parts.append("@HeaderMap Map<String, Object> headers)")
                else:
                    parts.append(")")
            elif param.type == "formal_parameter":
                for arg in param.children:
                    if arg.type == "type_identifier":
                        text: str = arg.text.decode("utf8")
                        if text == "File":
                            uses_file: bool = True
                            if replace_file:
                                text = "MetadataSource"
                        parts.append(text)
                        parts.append(" ")
                    else:
                        parts.append(arg.text.decode("utf8"))
                        parts.append(" ")
            else:
                parts.append(param.text.decode("utf8"))
                parts.append(" ")
    else:
        parts.append(node.text.decode("utf8"))
        parts.append(" ")
    return uses_file


def rewrite_api(api: Path, java_parser: Parser, java_language: Language) -> None:
    """
    Rewrites api file.
    :param api: Api file to rewrite
    :param java_parser: Java parser
    :param java_language: Java language
    :return: None
    """
    src: str = api.read_text()
    tree = java_parser.parse(src.encode("utf8"))
    imports = (
        "\nimport feign.HeaderMap;\n"
        "import java.io.InputStream;\n"
        "import java.io.OutputStream;\n"
        "import pl.com.rszewczyk.lib.interfaces.MetadataSource;\n"
    )
    src: str = re.sub("(package [^;]+;)", "\\1" + imports, src)

    root: Node = tree.root_node
    method_query: Query = java_language.query("(method_declaration) @capture")
    nodes_dict = method_query.captures(root)
    methods = []
    for _, nodes in nodes_dict.items():
        for node in nodes:
            with_file, body = modify_method(node, with_headers=True, replace_file=False)
            methods.append(body)
            methods.append("\n")
            if with_file:
                _, body = modify_method(node, with_headers=True, replace_file=True)
                methods.append(body)
                methods.append("\n")
                _, body = modify_method(node, with_headers=False, replace_file=True)
                methods.append(body)
                methods.append("\n")

    generated_code: str = "".join(methods)
    index: int = src.rfind("}")
    src = src[: index - 1] + generated_code + src[index:]
    api.write_text(src)


if __name__ == "__main__":
    main()
